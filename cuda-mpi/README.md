# about

MPI and CUDA in a docker container for GitLab CI tests.

# install

```bash
pip install -r requirements.txt
```

# build container

```bash
python recipe_docker.py > Dockerfile
docker build -t cuda_mpi:latest .
```

# run and test container

```bash
docker run --runtime=nvidia -it cuda_mpi:latest
mpirun --allow-run-as-root mpi_bandwidth
mpirun --allow-run-as-root /cuda_mpi_example/simpleMPI
```
