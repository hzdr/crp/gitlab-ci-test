"""
MPI Bandwidth
original from: https://github.com/NVIDIA/hpc-container-maker/blob/e3aa46e04cd28d7d7bfe5bc5df5d917067185f0a/recipes/mpi_bandwidth.py
Contents:
  Ubuntu 16.04
  GNU compilers (upstream)
  Mellanox OFED
  OpenMPI version 4.0.1
"""

import hpccm
from hpccm.primitives import baseimage, shell, copy
from hpccm.building_blocks.gnu import gnu
from hpccm.building_blocks.openmpi import openmpi
from hpccm.building_blocks.packages import packages
from hpccm.building_blocks.mlnx_ofed import mlnx_ofed

def main():
    hpccm.config.set_container_format('docker')
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image='nvidia/cuda:10.1-devel-ubuntu18.04')

    Stage0 += packages(ospackages=['software-properties-common', 'locales', 'locales-all'])
    # set language to en_US.UTF-8 to avoid some problems with some tools
    Stage0 += shell(commands=['locale-gen en_US.UTF-8', 'update-locale LANG=en_US.UTF-8'])
    Stage0 += shell(commands=['add-apt-repository ppa:ubuntu-toolchain-r/test',
                              'apt update'])

    # GNU compilers
#    Stage0 += gnu(version='7', fortran=False)

    # Mellanox OFED
    Stage0 += mlnx_ofed()

    # OpenMPI
    Stage0 += openmpi(cuda=True, version='4.0.1')

    # MPI Bandwidth
    Stage0 += shell(commands=['wget -q -nc --no-check-certificate -P /var/tmp https://computing.llnl.gov/tutorials/mpi/samples/C/mpi_bandwidth.c',
                              'mpicc -o /usr/local/bin/mpi_bandwidth /var/tmp/mpi_bandwidth.c'])

    Stage0 += copy(src='cuda_mpi_example/', dest='/cuda_mpi_example')
    Stage0 += shell(commands=['cd cuda_mpi_example',
                              'make',
                              'cd -'])

    print(Stage0.__str__())

if __name__ == '__main__':
    main()
