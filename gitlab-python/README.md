# preparation

1. Create a project access token (Settings->Access Tokens) with the permission `api`, `read_registry` and `write_registry`.
2. Set the token as variable with name `PRIVATE_JOB_TOKEN` in the CI (Settings->CI/CD). Don't forget to set `Mask variable` flag for security reasons.
