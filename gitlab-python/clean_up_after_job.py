import gitlab
import os

if __name__ == "__main__":
    for env_var in ["CI_SERVER_URL", "PRIVATE_JOB_TOKEN", "CI_PROJECT_ID"]:
        if not env_var in os.environ:
            print(f"{env_var} environment variable is not defined")
            exit(1)

    print(f"CI_SERVER_URL: {os.environ['CI_SERVER_URL']}")
    print(f"CI_PROJECT_ID: {os.environ['CI_PROJECT_ID']}")

    gl = gitlab.Gitlab(
        url=os.environ["CI_SERVER_URL"],
        private_token=os.environ["PRIVATE_JOB_TOKEN"],
    )

    gl.auth()
    print("login successful")

    project = gl.projects.get(os.environ["CI_PROJECT_ID"])

    print(f"project name: {project.attributes['name']}")
    print(f"project url:  {project.attributes['web_url']}")

    repositories = project.repositories.list(get_all=True)

    if not isinstance(repositories, list):
        repositories = [repositories]

    for repo in repositories:
        print(repo.attributes["name"])
        for tag in repo.tags.list():
            print(f"  {tag.attributes['name']}")
