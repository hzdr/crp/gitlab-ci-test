import os
from typing import List


def get_job_list(line_prefix: str) -> List[str]:
    """Grep line in commit message starting with the prefix and split up
    the following entries by the comma. Returns the entries in a list.

    e.g. prefix: job1, job2, job3
    return: ["job1", "job2", "job3"]

    :param line_prefix: line prefix
    :type line_prefix: str
    :returns: list of entries or empty list, if message variable does
    not exist or no line with prefix exists.
    :rtype: List[str]

    """
    jobs: List[str] = []
    if not os.getenv("CI_COMMIT_MESSAGE"):
        return jobs

    for line in os.getenv("CI_COMMIT_MESSAGE").split("\n"):
        if line.startswith(line_prefix):
            # remove line prefix, trim line string and
            # split string by comma
            jobs += line[len(line_prefix) :].strip().split(",")

    # trim all entries in the list
    jobs = [s.strip() for s in jobs]

    return jobs


def main():
    print("ci-only: ")
    for job in get_job_list("ci-only:"):
        print(f"  ${job}")

    print("\nci-prefer: ")
    for job in get_job_list("ci-prefer:"):
        print(f"  ${job}")


if __name__ == "__main__":
    main()
