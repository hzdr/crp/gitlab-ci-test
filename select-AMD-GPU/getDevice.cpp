#include <iostream>
#include <hip/hip_runtime.h>

inline void checkHip(hipError_t err){
  if (err != hipSuccess) {
    std::cout << "Error code: " << err << std::endl
	      << hipGetErrorName(err) << std::endl
	      << hipGetErrorString(err) << std::endl;
    exit(1);
  }
}

int main(){
  std::cout << "Hello Hip World" << std::endl;

  int numDevices = -1;
  checkHip(hipGetDeviceCount(&numDevices));
  std::cout << "Number of Devices: " << numDevices << std::endl << std::endl;

  hipDeviceProp_t prop;
  for(int i = 0; i < numDevices; ++i){
    std::cout << "========== GPU Nr. " << i << " ==========" << std::endl;
    checkHip(hipGetDeviceProperties(&prop, i));
    std::cout << "Name: " << prop.name << std::endl
	      << "PICe ID (Bus:Device): " << prop.pciBusID << ":" << prop.pciDeviceID << std::endl
	      << "totalGlobalMem: " << prop.totalGlobalMem / 1024 / 1024 << " MB" << std::endl
	      << "gcnArch: " << prop.gcnArch <<  std::endl;
    std::cout << std::endl;
  }
  return 0;
}
