import yaml, sys, os
from typing import Dict


def main():
    s = yaml.dump({"stages": ["builda", "buildb"]}) + "\n"
    i = 1

    for b in os.environ:
        if b.startswith("BUILD_"):
            name = b[len("BUILD_") :]
            build_command = os.environ[b]
            s += (
                yaml.dump(
                    job_a(
                        name=name + "_job" + str(i) + "a", build_command=build_command
                    )
                )
                + "\n"
            )
            s += (
                yaml.dump(
                    job_b(
                        name=name + "_job" + str(i) + "b",
                        build_command=build_command,
                        dependens=name + "_job" + str(i) + "a",
                    )
                )
                + "\n"
            )
            ++i

    print(s)


def base_job(name: str, build_command: str) -> Dict:
    return {
        name: {
            "stage": "build",
            "image": "docker:latest",
            "services": ["docker:dind"],
            "script": [
                'echo "build -> ' + name + " -> " + build_command + '" >> build.txt'
            ],
        }
    }


def job_a(name: str, build_command: str) -> Dict:
    base = base_job(name, build_command)
    base[name]["stage"] = "builda"
    base[name]["artifacts"] = {"paths": ["build.txt"], "expire_in": "1 week"}
    return base


def job_b(name: str, build_command: str, dependens: str) -> Dict:
    base = base_job(name, build_command)
    base[name]["stage"] = "buildb"
    base[name]["after_script"] = ["cat build.txt"]
    base[name]["dependencies"] = [dependens]
    return base


if __name__ == "__main__":
    main()
