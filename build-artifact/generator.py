import yaml, sys
from typing import Dict, List, Optional
from typeguard import typechecked

container_version: str = "1.4"


class TopLevelSpaceDumper(yaml.SafeDumper):
    # insert blank lines between top-level objects
    # inspired by https://stackoverflow.com/a/44284819/3786245
    def write_line_break(self, data=None):
        super().write_line_break(data)

        if len(self.indents) == 1:
            super().write_line_break()


@typechecked
def base_job(
    name: str,
    stage: str,
    script: List[str],
    container_suffix: str,
    tags: List[str],
    artifact_sufix: Optional[str] = None,
    needs: Optional[List[str]] = None,
) -> Dict:
    job = {
        name: {
            "stage": stage,
            "image": f"registry.gitlab.com/hzdr/crp/alpaka-group-container/alpaka-ci-{container_suffix}:{container_version}",
            "script": script,
        }
    }

    if artifact_sufix:
        job[name].update(
            {
                "artifacts": {
                    "name": f"$CI_COMMIT_REF_NAME-{artifact_sufix}",
                    "paths": ["build/"],
                    "expire_in": "1 day",
                }
            }
        )

    if needs:
        job[name].update({"needs": needs})

    if tags:
        job[name].update({"tags": tags})

    return job


@typechecked
def cpu_job(number: int) -> Dict:
    return base_job(
        name=f"CPU{number}",
        stage="build",
        script=[
            "mkdir build",
            f"g++ build-artifact/hello{number}.cpp -o build/hello",
            "./build/hello",
        ],
        container_suffix="gcc",
        artifact_sufix=f"P{number}",
        tags=["x86_64", "cpuonly"],
    )


@typechecked
def cuda_job(number: int) -> Dict:
    jobs = base_job(
        name=f"buildCUDA{number}",
        stage="build",
        script=[
            "mkdir build",
            "nvcc build-artifact/hello_cuda.cu -o build/hello",
        ],
        container_suffix="cuda113",
        artifact_sufix=f"P{number}",
        tags=["x86_64", "cpuonly"],
    )

    jobs.update(
        base_job(
            name=f"testCUDA{number}",
            stage="test",
            script=[
                "./build/hello",
            ],
            container_suffix="cuda113",
            needs=[f"buildCUDA{number}"],
            tags=["x86_64", "cuda"],
        )
    )

    return jobs


@typechecked
def hip_job(number: int) -> Dict:
    jobs = base_job(
        name=f"buildHIP{number}",
        stage="build",
        script=[
            "mkdir build",
            # gfx803 = Radeon R9 Fury, gfx900 = Vega 64, gfx906 = Redeon VII
            'hipcc build-artifact/hello_hip.cpp --offload-arch="gfx803,gfx900,gfx906" -o build/hello',
        ],
        container_suffix="rocm4.3",
        artifact_sufix=f"P{number}",
        tags=["x86_64", "cpuonly"],
    )

    jobs.update(
        base_job(
            name=f"testHIP{number}",
            stage="test",
            script=[
                "./build/hello",
            ],
            container_suffix="rocm4.3",
            needs=[f"buildHIP{number}"],
            tags=["x86_64", "rocm"],
        )
    )

    return jobs


def main():
    ci = {"stages": ["build", "test"]}

    ci.update(cpu_job(1))
    ci.update(cpu_job(2))
    ci.update(hip_job(3))
    ci.update(cuda_job(4))
    ci.update(cuda_job(5))
    ci.update(cuda_job(6))
    ci.update(cuda_job(7))
    ci.update(cuda_job(8))


    yaml.dump(ci, sys.stdout, Dumper=TopLevelSpaceDumper)


if __name__ == "__main__":
    main()
