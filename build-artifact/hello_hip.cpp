// This is the REAL "hello world" for HIP!
// It takes the string "Hello ", prints it, then passes it to HIP with an array
// of offsets. Then the offsets are added in parallel to produce the string "World!"
// By Ingemar Ragnemalm 2010

#include <stdio.h>
#include "hip/hip_runtime.h"

const int N = 16;
const int blocksize = 16;

__global__
void hello(char *a, int *b)
{
        a[hipThreadIdx_x] += b[hipThreadIdx_x];
}

int main()
{
        char a[N] = "Hello \0\0\0\0\0\0";
        int b[N] = {15, 10, 6, 0, -11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        char *ad;
        int *bd;
        const int csize = N*sizeof(char);
        const int isize = N*sizeof(int);

        printf("HIP: %s", a);

        hipMalloc( (void**)&ad, csize );
        hipMalloc( (void**)&bd, isize );
        hipMemcpy( ad, a, csize, hipMemcpyHostToDevice );
        hipMemcpy( bd, b, isize, hipMemcpyHostToDevice );

        dim3 dimBlock( blocksize, 1 );
        dim3 dimGrid( 1, 1 );
	hipLaunchKernelGGL(hello,
			dim3(dimGrid), dim3(dimBlock), 0, 0,
			ad, bd);
        hipMemcpy( a, ad, csize, hipMemcpyDeviceToHost );
        hipFree( ad );
        hipFree( bd );

        printf("%s\n", a);
        return EXIT_SUCCESS;
}
